<?php
    session_start();
    
    //DBとの連携確認
     $link = mysqli_connect("localhost","root","root","twclone");
    //サーバー名(ホスト)、データベースユーザ名、パスワード,データベース名
    if(mysqli_connect_error()){
        die("DBへの接続に失敗しました");
    }

    //入力の空白チェック
    $email = "";
    $password = "";

    if(array_key_exists('email',$_POST) AND
      array_key_exists('password',$_POST)){
        if($_POST['email'] == null){
            $email = "メールアドレスを入力してください";
        } elseif ($_POST['password'] == null){
            $password = "パスワードを入力してください";
        } else {
            //入力されたアドレスとパスがDBにあるかどうか
            //まずはパスがDBに存在するか
            $query = "SELECT `id` FROM `users` WHERE `email` = '".mysqli_real_escape_string($link,$_POST['email'])."'";
            $result = mysqli_query($link,$query);
            $query2 = "SELECT `id` FROM `users` WHERE `password` = '".mysqli_real_escape_string($link,$_POST['password'])."'";
            $result2 = mysqli_query($link,$query2);    
            
            if(mysqli_num_rows($result) != 1){
                $email = "このメールアドレスは登録されていません";      
            } elseif (mysqli_num_rows($result2) == 0){
                $email = "このパスワードは登録されていません";      
            } else {
                $_SESSION["email"] = $_POST['email'];
                header("Location: post.php");
            }
            
        }
    }


    //天気１APIを作成    

    if(array_key_exists('city',$_GET)){
        $urlContents = file_get_contents("https://api.openweathermap.org/data/2.5/weather?q=".$_GET['city']."&appid=df119dbf391b8850e38a84998681352e");
        $weatherArray = json_decode($urlContents,true);
        
        $weather = $_GET['city']."の天気は、".$weatherArray['weather'][0]['main'].",".$weatherArray['weather'][0]['description']."です。";
        
    }
    
  
    


?>


<html>
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
        
        <title>HTtwitter</title>
        <link rel="stylesheet" href="tw-login.css">
    </head>

    
    

    <body]>
       
        <h1>HTtwitterログイン</h1>

        <a class="btn btn-primary" href="tw-newMember.php" role="button">新規登録はこちらから</a>
        <br><br><br>
        
        <div class="container">
            <form method="post">
                <div class="form-group row">
                    <label for="exampleInputEmail" class="col-sm-2 col-form-label">Email</label>
                    <div class="col-sm-10">
                        <input type="email" class="form-control" name="email" placeholder="Email">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputPassword" class="col-sm-2 col-form-label">Password</label>
                    <div class="col-sm-10">
                        <input type="password" class="form-control" name="password" placeholder="Password">
                    </div>
                </div>
                <div class="loginForm">
                    <button type="submit" class="btn btn-primary">ログイン</button>
                </div>
            </form>
        </div>
        
        
        
        
        
        

        <p class="errorMessage"><?php echo $email; ?></p>
        <p class="errorMessage"><?php echo $password; ?></p>
        
        <div class="container">
            <form method="get">
                <div class="form-group row">
                    <label for="exampleInputEmail" class="col-sm-3 col-form-label">天気を調べる</label>
                    <div class="col-sm-6">
                        <input type="text" class="form-control" name="city" placeholder="都市、国(半角英で入力)">
                    </div>
                    <div class="loginForm">
                        <button type="submit" class="btn btn-primary">調べる</button>
                    </div>
                </div>
            </form>
        </div>
        
        
        <div class="container">
            <div class="d-lg-none">
                <h1 class="display-4"><?php if($_GET['city'] AND $weatherArray){
                        echo $weather;
                    }elseif($_GET['city'] == null){
                        echo "";
                    }else{
                        echo "該当地域がありません";} ?>
                </h1>
            </div>
        </div>

        
        
        <!-- Optional JavaScript -->
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
        
    </body]>
</html>