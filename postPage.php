<?php
     session_start();
    //DBとの連携確認
     $link = mysqli_connect("localhost","root","root","twclone");
    //サーバー名(ホスト)、データベースユーザ名、パスワード,データベース名
    if(mysqli_connect_error()){
        die("DBへの接続に失敗しました");
    }
    
    //ログインした時のemailでログイン者の名前を$name[0]に格納
    $query = "SELECT `name` FROM `users` WHERE `email` = '".mysqli_real_escape_string($link,$_SESSION['email'])."'";
    $result = mysqli_query($link,$query);
    $name = mysqli_fetch_array($result);
    //$name[0] = ログインした人のname
    
    //DBにINSERTするのは、$postContent、$name[0]
    
    $postContent = "";
    
    if(array_key_exists('postContent',$_POST)){
        if($_POST['postContent'] == null){
            $postContentError = "投稿内容を入力してください";
        }else{
            $query = "INSERT INTO `post` (`posting`, `name`) VALUES ('".mysqli_real_escape_string($link,$_POST['postContent'])."','".mysqli_real_escape_string($link,$name[0])."')";
            mysqli_query($link,$query);
            //投稿ができれば投稿表示画面へ
            header("Location: post.php");
        }
    }

?>


<html lang="ja">
     <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">

        <title>投稿内容</title>
      </head>
      <body>
        <h1>投稿記事</h1>
          
        <a href="tw-login.php">ログインへ</a>
        <p><a href="tw-newMember.php">登録画面へ</a></p>
        <p><a href="post.php">投稿表示画面</a></p>
        <form method="post">
            <textarea rows="10" cols="60" placeholder="投稿内容を記入"></textarea>
            <br>
            <button type="submit" class="btn btn-primary">登録</button>
        </form>
          
        <p><?php echo $postContentError ?></p>

        <!-- Optional JavaScript -->
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
      </body>
</html>
