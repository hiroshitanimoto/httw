<?php 
    session_start();
    
    //DBとの連携確認
    $link = mysqli_connect("localhost","root","root","twclone");
    //サーバー名(ホスト)、データベースユーザ名、パスワード,データベース名
    if(mysqli_connect_error()){
        die("DBへの接続に失敗しました");
    }
    //入力欄の空白チェック
    $name = "";
    $email = "";
    $password = "";
    $old ="";
    $address = "";
    $profile = "";

    if(array_key_exists('email',$_POST) AND array_key_exists('password',$_POST) AND array_key_exists('name',$_POST)){
        if($_POST['name'] == null){
            $name = "名前を入力してください";
        } elseif($_POST['email'] == null){
            $email =  "メールアドレスを入力してください";
        } elseif($_POST['password'] == null){
            $password =  "パスワードを入力してください";
        } else {
            $query = "SELECT `id` FROM `users` WHERE `email` = '".mysqli_real_escape_string($link,$_POST['email'])."'";
            $result = mysqli_query($link,$query);
            
            $queryName = "SELECT `id` FROM `users` WHERE `name` =       '".mysqli_real_escape_string($link,$_POST['name'])."'";
            $resultName = mysqli_query($link,$queryName);
            
            if(mysqli_num_rows($result) > 0){
                $email = "このメールアドレスは既に使用されています";
            } elseif(mysqli_num_rows($resultName) > 0){
                $name = "この名前は既に使用されています";
            } else {
                $query = "INSERT INTO `users` (`name`, `email`, `password`, `old`, `sex`, `address`, `profile`) VALUES ('".mysqli_real_escape_string($link,$_POST['name'])."', '".mysqli_real_escape_string($link,$_POST['email'])."', '".mysqli_real_escape_string($link,$_POST['password'])."', '".mysqli_real_escape_string($link,$_POST['old'])."',
                '".mysqli_real_escape_string($link,$_POST['sex'])."', '".mysqli_real_escape_string($link,$_POST['address'])."',
                '".mysqli_real_escape_string($link,$_POST['profile'])."')";
                if(mysqli_query($link,$query)){
                    $_SESSION["name"] = $_POST['name'];
                    $_SESSION["email"] = $_POST['email'];
                    header("Location: postPage.php");
                } else {
                    echo "エラーです";
                }
            } 
        }
    }
?>

<html lang="ja">
    <head>
        <link rel="stylesheet" href="tw-newMember.css">
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
        
        <title>新規登録</title>
    </head>

    <body>
        <header>
             <h1>新しく登録する</h1>
        </header>
          
         <div class="container">
            <form method="post">   
                 <div class="form-group row">
                    <label for="exampleInputName" class="col-sm-2 col-form-label">名前</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="name" placeholder="名前" value="<?php if(!empty($_POST['name'])){echo $_POST['name'];} ?>">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="exampleInputName" class="col-sm-2 col-form-label">性別</label>
                    <div class="col-sm-10">
                        <select class="form-control" name="sex">
                            <option>男性</option>
                            <option>女性</option>
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="exampleInputName" class="col-sm-2 col-form-label">年齢</label>
                    <div class="col-sm-10">
                        <input type="number" class="form-control" name="old" placeholder="年齢" value="<?php if(!empty($_POST['old'])){echo $_POST['old'];} ?>">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="exampleInputName" class="col-sm-2 col-form-label">都道府県</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="address" placeholder="都道府県" value="<?php if(!empty($_POST['address'])){echo $_POST['address'];} ?>">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="exampleInputEmail" class="col-sm-2 col-form-label">Email</label>
                    <div class="col-sm-10">
                        <input type="email" class="form-control" name="email" placeholder="Email" value="<?php if(!empty($_POST['email'])){echo $_POST['email'];} ?>">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputPassword" class="col-sm-2 col-form-label">Password</label>
                    <div class="col-sm-10">
                        <input type="password" class="form-control" name="password" placeholder="Password">
                    </div>
                </div>
                <div class="form-group">
                    <label for="exampleFormControlTextarea1">プロフィール</label>
                    <textarea class="form-control" rows="3" placeholder="趣味や仕事など" name="profile"><?php if(!empty($_POST['profile'])){echo $_POST['profile'];} ?></textarea>
                </div>
                <div class="form-group form-check">
                    <input type="checkbox" class="form-check-input" id="exampleCheck1">
                    <label class="form-check-label" for="exampleCheck1">登録内容は上記でよろしいですか？</label>
                </div> 
                <button type="submit" class="btn btn-primary">登録</button>
            </form>
        </div>    
        
        <p class="errorMessage"><?php echo $name.$email.$password.$old.$address.$profile; ?></p>
        

        <a class="btn btn-primary" href="tw-login.php" role="button">ログイン画面へ</a>
        
        <!-- Optional JavaScript -->
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
    </body>
</html>