<?php
    session_start();
    //DBとの連携確認
     $link = mysqli_connect("localhost","root","root","twclone");
    //サーバー名(ホスト)、データベースユーザ名、パスワード,データベース名
    if(mysqli_connect_error()){
        die("DBへの接続に失敗しました");
    }

    $old = "";
    $sex = "";
    $address = "";
    $profile = "";

    //今欲しいのはログインした人間のemailなのでそれがこの画面まで保持できてるかの確認
    $query = "SELECT `name` FROM `users` WHERE `email` = '".mysqli_real_escape_string($link,$_SESSION['email'])."'";
    $result = mysqli_query($link,$query);
    $name = mysqli_fetch_array($result);
    print_r($name);
    

    if(array_key_exists('old',$_POST) AND array_key_exists('address',$_POST) AND array_key_exists('profile',$_POST)){
        if($_POST['old'] == null){
            $old = "年齢を記入してください";
        } elseif ($_POST ['address'] == null){
            $address = "住所（都道府県のみ)を入力してください";
        } elseif ($_POST['profile'] == null){
            $profile = "プロフィールを入力してください";
        } else {
            $query = "INSERT INTO `users` (`old`, `sex`, `address`, `profile`) VALUES ('".mysqli_real_escape_string($link,$_POST['old'])."','".mysqli_real_escape_string($link,$_POST['sex'])."','".mysqli_real_escape_string($link,$_POST['address'])."','".mysqli_real_escape_string($link,$_POST['profile'])."') WHERE `email` = '".mysqli_real_escape_string($link,$_SESSION['email'])."'" ;
            if(mysqli_query($link,$query)){
                header("LOCATION: post.php");
            }
        }
    }



?>

<html>
    
    
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">

        <title>プロフィール変更画面</title>
    </head>
    <body>
        <a href="post.php">投稿表示画面</a>
        <br><br>
        
         <div class="container">
            <form method="post">
              <div class="form-group">
                <label for="exampleFormControlInput1">年齢(半角)</label>
                <input type="number" class="form-control"  name="old">
              </div>
              <div class="form-group">
                <label for="exampleFormControlSelect1">性別</label>
                <select class="form-control" name="sex">
                  <option>男性</option>
                  <option>女性</option>
                </select>
              </div>
              <div class="form-group">
                <label for="exampleFormControlInput1">都道府県</label>
                <input type="text" class="form-control" name="address">
              </div>
              <div class="form-group">
                <label for="exampleFormControlTextarea1">プロフィール</label>
                <textarea class="form-control" rows="3" placeholder="趣味や仕事など" name="profile"></textarea>
              </div>
                <br>
                <button type="submit" class="btn btn-primary">登録</button>
            </form>
        </div>
    
        <!-- Optional JavaScript -->
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script> 
    </body>
    
</html>